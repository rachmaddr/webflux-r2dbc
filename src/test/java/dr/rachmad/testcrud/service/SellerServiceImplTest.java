package dr.rachmad.testcrud.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

import dr.rachmad.testcrud.configuration.DatabaseConfig;
import dr.rachmad.testcrud.configuration.GeneralConfig;
import dr.rachmad.testcrud.controller.ProductController;
import dr.rachmad.testcrud.controller.SellerController;
import dr.rachmad.testcrud.dao.SellerRepository;
import dr.rachmad.testcrud.dto.SellerDto;
import dr.rachmad.testcrud.entity.Seller;
import dr.rachmad.testcrud.request.SellerRequest;
import dr.rachmad.testcrud.services.ProductService;
import dr.rachmad.testcrud.services.SellerService;
import dr.rachmad.testcrud.services.SellerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.transaction.ReactiveTransactionManager;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@WebFluxTest(excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
        classes = {ProductService.class, SellerService.class,
            GeneralConfig.class, DatabaseConfig.class, ProductController.class,
            SellerController.class})
})
public class SellerServiceImplTest {
  @Mock
  ReactiveTransactionManager transactionManager;

  @Mock
  R2dbcEntityTemplate r2dbcEntityTemplate;

  @Mock
  ProductService productService;

  @Mock
  SellerRepository sellerRepository;

  @Mock
  ModelMapper modelMapper;

  ModelMapper modelMapperMocker;

  @BeforeEach
  public void init(){
    modelMapperMocker = new ModelMapper();
    modelMapperMocker.getConfiguration()
        .setMatchingStrategy(MatchingStrategies.STRICT);
    modelMapperMocker.getConfiguration()
        .setSkipNullEnabled(true);
  }

  @InjectMocks
  private SellerServiceImpl sellerServiceImpl;

  @Test
  public void testCreate() {
    Seller param = Seller.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();

    Seller result = Seller.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    result.setId(1l);

    given(r2dbcEntityTemplate.insert(param))
        .willReturn(Mono.just(result));

    StepVerifier.create(sellerServiceImpl.insert(param))
        .expectNext(result)
        .verifyComplete();
  }

  @Test
  public void testUpdate() {
    Seller param = Seller.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    param.setId(1l);

    Seller resultOri = Seller.builder()
        .alamat("DPK")
        .kota("Jakarta")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    resultOri.setId(1l);

    Seller result = Seller.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    result.setId(1l);

    given(sellerRepository.findById(1l))
        .willReturn(Mono.just(resultOri));

    given(sellerRepository.save(result))
        .willReturn(Mono.just(result));

    StepVerifier.create(sellerServiceImpl.save(param))
        .expectNext(result)
        .verifyComplete();
  }

  @Test
  public void testGetByKota() {
    Seller result = Seller.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    result.setId(1l);

    SellerDto resultDto = SellerDto.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    resultDto.setId(1l);

    SellerRequest request = new SellerRequest().toBuilder()
        .kota("De")
        .build();
    request.setPageNumber(1);
    request.setPageSize(1);

    given(r2dbcEntityTemplate.select(any(), eq(Seller.class)))
        .willReturn(Flux.just(result));

    given(modelMapper.map(any(), eq(SellerDto.class)))
        .will(invocationOnMock -> {
          Seller resultTemp = invocationOnMock.getArgument(0);
          return modelMapperMocker.map(resultTemp, SellerDto.class);
        });

    StepVerifier.create(sellerServiceImpl.getAll(request))
        .expectNext(resultDto)
        .verifyComplete();
  }

  @Test
  public void testGetByName() {
    Seller result = Seller.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    result.setId(1l);

    SellerDto resultDto = SellerDto.builder()
        .alamat("DPK")
        .kota("Depok")
        .nama("Rahmad")
        .nomorTelp("0832433453443")
        .build();
    resultDto.setId(1l);

    SellerRequest request = new SellerRequest().toBuilder()
        .nama("Ra")
        .build();
    request.setPageNumber(1);
    request.setPageSize(1);

    given(r2dbcEntityTemplate.select(any(), eq(Seller.class)))
        .willReturn(Flux.just(result));

    given(modelMapper.map(any(), eq(SellerDto.class)))
        .will(invocationOnMock -> {
          Seller resultTemp = invocationOnMock.getArgument(0);
          return modelMapperMocker.map(resultTemp, SellerDto.class);
        });

    StepVerifier.create(sellerServiceImpl.getAll(request))
        .expectNext(resultDto)
        .verifyComplete();
  }

  @Test
  public void testGetCount() {
    SellerRequest request = new SellerRequest().toBuilder()
        .nama("Ra")
        .build();

    given(r2dbcEntityTemplate.count(any(), eq(Seller.class)))
        .willReturn(Mono.just(1l));

    StepVerifier.create(sellerServiceImpl.getCount(request))
        .expectNext(1l)
        .verifyComplete();
  }
}
