package dr.rachmad.testcrud.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

import dr.rachmad.testcrud.configuration.DatabaseConfig;
import dr.rachmad.testcrud.configuration.GeneralConfig;
import dr.rachmad.testcrud.controller.ProductController;
import dr.rachmad.testcrud.controller.SellerController;
import dr.rachmad.testcrud.dao.ProductRepository;
import dr.rachmad.testcrud.dto.ProductDto;
import dr.rachmad.testcrud.entity.Product;
import dr.rachmad.testcrud.entity.Seller;
import dr.rachmad.testcrud.request.ProductRequest;
import dr.rachmad.testcrud.services.ProductService;
import dr.rachmad.testcrud.services.ProductServiceImpl;
import dr.rachmad.testcrud.services.SellerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.transaction.ReactiveTransactionManager;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@WebFluxTest(excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
        classes = {ProductService.class, SellerService.class,
            GeneralConfig.class, DatabaseConfig.class, ProductController.class,
            SellerController.class})
})
public class ProductServiceImplTest {
  @Mock
  ReactiveTransactionManager transactionManager;

  @Mock
  R2dbcEntityTemplate r2dbcEntityTemplate;

  @Mock
  ProductRepository productRepository;

  @Mock
  SellerService sellerService;

  @Mock
  ModelMapper modelMapper;

  ModelMapper modelMapperMocker;

  @BeforeEach
  public void init(){
    modelMapperMocker = new ModelMapper();
    modelMapperMocker.getConfiguration()
        .setMatchingStrategy(MatchingStrategies.STRICT);
    modelMapperMocker.getConfiguration()
        .setSkipNullEnabled(true);
  }

  @InjectMocks
  private ProductServiceImpl productServiceImpl;

  @Test
  public void testCreate() {
    Product param = Product.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();

    Product result = Product.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    result.setId(1l);

    Seller resultSeller = Seller.builder()
        .alamat("Depok")
        .kota("Depok")
        .nama("Seller Depok")
        .build();
    resultSeller.setId(1l);

    given(r2dbcEntityTemplate.insert(param))
        .willReturn(Mono.just(result));

    given(sellerService.findById(1l))
        .willReturn(Mono.just(resultSeller));

    StepVerifier.create(productServiceImpl.insert(param))
        .expectNext(result)
        .verifyComplete();
  }

  @Test
  public void testUpdate() {
    Product param = Product.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    param.setId(1l);

    Product resultOri = Product.builder()
        .satuan("pack")
        .hargaSatuan(3500d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    resultOri.setId(1l);

    Product result = Product.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    result.setId(1l);

    given(productRepository.findById(1l))
        .willReturn(Mono.just(resultOri));

    given(productRepository.save(result))
        .willReturn(Mono.just(result));

    StepVerifier.create(productServiceImpl.save(param))
        .expectNext(result)
        .verifyComplete();
  }

  @Test
  public void testGetBySellerId() {
    Product result = Product.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    result.setId(1l);

    ProductDto resultDto = ProductDto.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    resultDto.setId(1l);

    ProductRequest request = new ProductRequest().toBuilder()
        .sellerId(1l)
        .build();
    request.setPageNumber(1);
    request.setPageSize(1);

    given(r2dbcEntityTemplate.select(any(), eq(Product.class)))
        .willReturn(Flux.just(result));

    given(modelMapper.map(any(), eq(ProductDto.class)))
        .will(invocationOnMock -> {
          Product resultTemp = invocationOnMock.getArgument(0);
          return modelMapperMocker.map(resultTemp, ProductDto.class);
        });

    StepVerifier.create(productServiceImpl.getAll(request))
        .expectNext(resultDto)
        .verifyComplete();
  }

  @Test
  public void testGetByName() {
    Product result = Product.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    result.setId(1l);

    ProductDto resultDto = ProductDto.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    resultDto.setId(1l);

    ProductRequest request = new ProductRequest().toBuilder()
        .nama("Ra")
        .build();
    request.setPageNumber(1);
    request.setPageSize(1);

    given(r2dbcEntityTemplate.select(any(), eq(Product.class)))
        .willReturn(Flux.just(result));

    given(modelMapper.map(any(), eq(ProductDto.class)))
        .will(invocationOnMock -> {
          Product resultTemp = invocationOnMock.getArgument(0);
          return modelMapperMocker.map(resultTemp, ProductDto.class);
        });

    StepVerifier.create(productServiceImpl.getAll(request))
        .expectNext(resultDto)
        .verifyComplete();
  }

  @Test
  public void testGetCount() {
    ProductRequest request = new ProductRequest().toBuilder()
        .nama("Ra")
        .build();

    given(r2dbcEntityTemplate.count(any(), eq(Product.class)))
        .willReturn(Mono.just(1l));

    StepVerifier.create(productServiceImpl.getCount(request))
        .expectNext(1l)
        .verifyComplete();
  }
}
