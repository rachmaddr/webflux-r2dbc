package dr.rachmad.testcrud.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

import dr.rachmad.testcrud.dto.ProductDto;
import dr.rachmad.testcrud.entity.Product;
import dr.rachmad.testcrud.request.ProductRequest;
import dr.rachmad.testcrud.services.ProductService;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = MainTestConfig.class)
public class ProductControllerTest {

  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private ProductService productService;
  @Autowired
  private ModelMapper modelMapper;

  @Test
  public void checkAPITest() {
    ProductDto resultDto = ProductDto.builder()
        .satuan("pack")
        .hargaSatuan(3000d)
        .nama("Rahmad")
        .sellerId(1l)
        .build();
    resultDto.setId(1l);

    ProductRequest request = new ProductRequest().toBuilder()
        .build();

    given(productService.getAll(request))
        .willReturn(Flux.just(resultDto));

    webTestClient.get()
        .uri("/product")
        .attribute("data.name", "Ra")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON)
        .expectBody();
  }
}
