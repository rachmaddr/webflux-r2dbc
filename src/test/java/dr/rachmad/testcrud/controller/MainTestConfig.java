package dr.rachmad.testcrud.controller;

import dr.rachmad.testcrud.services.ProductServiceImpl;
import dr.rachmad.testcrud.services.SellerServiceImpl;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@ComponentScan(basePackages = {
    "dr.rachmad.testcrud.controller",
    "dr.rachmad.testcrud.services"
}, excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
        classes = {ProductServiceImpl.class, SellerServiceImpl.class})
})
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.REGEX,
    pattern = "dr.rachmad.testcrud.configuration.*")})
public class MainTestConfig {
  @MockBean
  private ProductServiceImpl productServiceMock;

  @MockBean
  private SellerServiceImpl sellerServiceMock;

  @Bean
  public ModelMapper initModelMapper() {
    final ModelMapper modelMapper = new ModelMapper();
    modelMapper.getConfiguration()
        .setMatchingStrategy(MatchingStrategies.STRICT);
    modelMapper.getConfiguration()
        .setSkipNullEnabled(true);
    return modelMapper;
  }

  @Bean
  public ProductServiceImpl productService() {
    return productServiceMock;
  }

  @Bean
  public SellerServiceImpl sellerService() {
    return sellerServiceMock;
  }
}
