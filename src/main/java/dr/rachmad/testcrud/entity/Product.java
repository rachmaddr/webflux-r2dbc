package dr.rachmad.testcrud.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("PRODUCT_TBL")
public class Product extends BaseEntity implements Serializable {

  @Column("SELLER_ID")
  private Long sellerId;

  @Column("NAMA")
  private String nama;

  @Column("SATUAN")
  private String satuan;

  @Column("HARGA_SATUAN")
  private Double hargaSatuan;
}
