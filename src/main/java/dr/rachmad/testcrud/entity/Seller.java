package dr.rachmad.testcrud.entity;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table("SELLER_TBL")
public class Seller extends BaseEntity implements Serializable {

  @NotBlank
  @Column("NAMA")
  private String nama;

  @NotBlank
  @Column("ALAMAT")
  private String alamat;

  @NotBlank
  @Column("KOTA")
  private String kota;

  @NotBlank
  @Column("NOMOR_TELP")
  private String nomorTelp;

}
