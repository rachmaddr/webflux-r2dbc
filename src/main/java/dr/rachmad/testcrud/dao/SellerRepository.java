package dr.rachmad.testcrud.dao;

import dr.rachmad.testcrud.entity.Seller;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRepository extends ReactiveCrudRepository<Seller, Long> {

}
