package dr.rachmad.testcrud.dao;

import dr.rachmad.testcrud.entity.Product;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ProductRepository extends R2dbcRepository<Product, Long> {

  @Query("delete from PRODUCT where SELLER_ID = :sellerId")
  Mono<Void> deleteBySellerId(Long sellerId);
}