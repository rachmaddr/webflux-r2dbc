package dr.rachmad.testcrud.utils;

public interface ConstantResponse {

  public static final String SUCCESS_CODE = "SUCCESS";
  public static final String SUCCESS_VALUE = "success";
}
