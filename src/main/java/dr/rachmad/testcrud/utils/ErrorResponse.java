package dr.rachmad.testcrud.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
@ToString(callSuper = true)
public class ErrorResponse extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final HttpStatus httpStatus;
  private final Integer code;
  private final String message;

  public ErrorResponse(HttpStatus httpStatus, String message) {
    this(httpStatus, httpStatus.value(), message);
  }

}
