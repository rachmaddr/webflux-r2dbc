package dr.rachmad.testcrud.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDto implements Serializable {

  private Long id;
  private Long sellerId;
  private String nama;
  private String satuan;
  private Double hargaSatuan;
}
