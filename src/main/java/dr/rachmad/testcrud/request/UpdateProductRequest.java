package dr.rachmad.testcrud.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder(toBuilder = true)
public class UpdateProductRequest implements Serializable {

  @NotNull
  @Positive
  private Long id;

  @Positive(message = "sellerId tidak boleh kurang dari 0")
  private Long sellerId;

  @NotBlank
  private String nama;
  private String satuan;

  @Positive
  private Double hargaSatuan;

}
