package dr.rachmad.testcrud.request;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class CreateSellerRequest implements Serializable {

  private String nama;
  private String alamat;
  private String kota;

  private String nomorTelp;
}
