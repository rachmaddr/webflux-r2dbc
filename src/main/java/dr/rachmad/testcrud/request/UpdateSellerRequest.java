package dr.rachmad.testcrud.request;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class UpdateSellerRequest implements Serializable {

  @NotNull
  @Positive
  private Long id;
  private String nama;
  private String alamat;
  private String kota;

  private String nomorTelp;
}
