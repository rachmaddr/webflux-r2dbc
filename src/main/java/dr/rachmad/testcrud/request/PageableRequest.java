package dr.rachmad.testcrud.request;

import java.io.Serializable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.domain.Sort;

@Getter
@Setter
@ToString(callSuper = true)
public class PageableRequest implements Serializable {


  public static final Integer DEFAULT_PAGE_NUMBER = 1;
  public static final Integer DEFAULT_PAGE_SIZE = 30;
  public static final Sort.Direction DEFAULT_SORT_DIRECTION = Sort.Direction.ASC;

  @Min(1)
  @NotNull
  private Integer pageNumber = DEFAULT_PAGE_NUMBER;

  @Min(1)
  @NotNull
  private Integer pageSize = DEFAULT_PAGE_SIZE;

  public Integer getSkipRow() {
    return (pageNumber - 1) * pageSize;
  }
}
