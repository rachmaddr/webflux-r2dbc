package dr.rachmad.testcrud.controller;

import dr.rachmad.testcrud.dto.BaseResponse;
import dr.rachmad.testcrud.dto.ProductDto;
import dr.rachmad.testcrud.dto.SellerDto;
import dr.rachmad.testcrud.entity.Seller;
import dr.rachmad.testcrud.request.CreateSellerRequest;
import dr.rachmad.testcrud.request.SellerRequest;
import dr.rachmad.testcrud.request.UpdateSellerRequest;
import dr.rachmad.testcrud.services.SellerService;
import dr.rachmad.testcrud.utils.ConstantResponse;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController()
@Validated
@RequestMapping("/seller")
public class SellerController {

  @Autowired
  private SellerService sellerService;

  @Autowired
  private ModelMapper modelMapper;

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> addProduct(@RequestBody @Valid CreateSellerRequest param) {
    return Mono.just(param)
        .map(request -> modelMapper.map(request, Seller.class))
        .flatMap(sellerService::insert)
        .map(result -> {
          SellerDto dto = modelMapper.map(result, SellerDto.class);
          return new BaseResponse<SellerDto>().toBuilder()
              .status(HttpStatus.OK.value())
              .code(ConstantResponse.SUCCESS_CODE)
              .message(ConstantResponse.SUCCESS_VALUE)
              .data(dto)
              .build();
        });
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> editProduct(@RequestBody @Valid UpdateSellerRequest param) {
    return Mono.just(param)
        .map(request -> modelMapper.map(request, Seller.class))
        .flatMap(sellerService::save)
        .map(result -> {
          SellerDto dto = modelMapper.map(result, SellerDto.class);
          return new BaseResponse<SellerDto>().toBuilder()
              .status(HttpStatus.OK.value())
              .code(ConstantResponse.SUCCESS_CODE)
              .message(ConstantResponse.SUCCESS_VALUE)
              .data(dto)
              .build();
        });
  }

  @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> deleteProduct(@PathVariable Long id) {
    return sellerService.delete(id)
        .then(Mono.just(new BaseResponse<ProductDto>().toBuilder()
            .status(HttpStatus.OK.value())
            .code(ConstantResponse.SUCCESS_CODE)
            .message(ConstantResponse.SUCCESS_VALUE)
            .data(null)
            .build()));
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> allProduct(@Valid SellerRequest param) {
    return sellerService.getAll(param)
        .collectList()
        .map(list -> new BaseResponse<List<SellerDto>>().toBuilder()
            .status(HttpStatus.OK.value())
            .code(ConstantResponse.SUCCESS_CODE)
            .message(ConstantResponse.SUCCESS_VALUE)
            .data(list)
            .build());
  }

  @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<SellerDto> allProductStream(@Valid SellerRequest param) {
    return sellerService.getAll(param);
  }

  @GetMapping("/count")
  public Mono<BaseResponse> countProduct(@Valid SellerRequest param) {
    return sellerService.getCount(param)
        .map(count -> new BaseResponse<Long>().toBuilder()
            .status(HttpStatus.OK.value())
            .code(ConstantResponse.SUCCESS_CODE)
            .message(ConstantResponse.SUCCESS_VALUE)
            .data(count)
            .build());
  }
}
