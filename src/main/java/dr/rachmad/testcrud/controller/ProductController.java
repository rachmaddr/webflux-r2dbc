package dr.rachmad.testcrud.controller;

import dr.rachmad.testcrud.dto.BaseResponse;
import dr.rachmad.testcrud.dto.ProductDto;
import dr.rachmad.testcrud.entity.Product;
import dr.rachmad.testcrud.request.CreateProductRequest;
import dr.rachmad.testcrud.request.ProductRequest;
import dr.rachmad.testcrud.request.UpdateProductRequest;
import dr.rachmad.testcrud.services.ProductService;
import dr.rachmad.testcrud.utils.ConstantResponse;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController()
@Validated
@RequestMapping("/product")
public class ProductController {

  @Autowired
  private ProductService productService;

  @Autowired
  private ModelMapper modelMapper;

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> addProduct(@RequestBody @Valid CreateProductRequest param) {
    return Mono.just(param)
        .map(request -> modelMapper.map(request, Product.class))
        .flatMap(productService::insert)
        .map(result -> {
          ProductDto dto = modelMapper.map(result, ProductDto.class);
          return new BaseResponse<ProductDto>().toBuilder()
              .status(HttpStatus.OK.value())
              .code(ConstantResponse.SUCCESS_CODE)
              .message(ConstantResponse.SUCCESS_VALUE)
              .data(dto)
              .build();
        });
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> editProduct(@RequestBody @Valid UpdateProductRequest param) {
    return Mono.just(param)
        .map(request -> modelMapper.map(request, Product.class))
        .flatMap(productService::save)
        .map(result -> {
          ProductDto dto = modelMapper.map(result, ProductDto.class);
          return new BaseResponse<ProductDto>().toBuilder()
              .status(HttpStatus.OK.value())
              .code(ConstantResponse.SUCCESS_CODE)
              .message(ConstantResponse.SUCCESS_VALUE)
              .data(dto)
              .build();
        });
  }

  @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> deleteProduct(@PathVariable Long id) {
    return productService.delete(id)
        .then(Mono.just(new BaseResponse<ProductDto>().toBuilder()
            .status(HttpStatus.OK.value())
            .code(ConstantResponse.SUCCESS_CODE)
            .message(ConstantResponse.SUCCESS_VALUE)
            .data(null)
            .build()));
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<BaseResponse> allProduct(@Valid ProductRequest param) {
    return productService.getAll(param)
        .collectList()
        .map(list -> new BaseResponse<List<ProductDto>>().toBuilder()
            .status(HttpStatus.OK.value())
            .code(ConstantResponse.SUCCESS_CODE)
            .message(ConstantResponse.SUCCESS_VALUE)
            .data(list)
            .build());
  }

  @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<BaseResponse> allProductStreams(@Valid ProductRequest param) {
    return productService.getAll(param)
        .map(result -> {
          ProductDto dto = modelMapper.map(result, ProductDto.class);
          return new BaseResponse<ProductDto>().toBuilder()
              .status(HttpStatus.OK.value())
              .code(ConstantResponse.SUCCESS_CODE)
              .message(ConstantResponse.SUCCESS_VALUE)
              .data(dto)
              .build();
        });
  }

  @GetMapping("/count")
  public Mono<BaseResponse> countProduct(@Valid ProductRequest param) {
    return productService.getCount(param)
        .map(count -> new BaseResponse<Long>().toBuilder()
            .status(HttpStatus.OK.value())
            .code(ConstantResponse.SUCCESS_CODE)
            .message(ConstantResponse.SUCCESS_VALUE)
            .data(count)
            .build());
  }
}
