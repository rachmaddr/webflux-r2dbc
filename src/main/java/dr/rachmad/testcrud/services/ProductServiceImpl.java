package dr.rachmad.testcrud.services;

import dr.rachmad.testcrud.dao.ProductRepository;
import dr.rachmad.testcrud.dto.ProductDto;
import dr.rachmad.testcrud.entity.Product;
import dr.rachmad.testcrud.request.ProductRequest;
import dr.rachmad.testcrud.utils.ErrorResponse;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {
  @Autowired
  ProductRepository productRepository;

  @Autowired
  R2dbcEntityTemplate r2dbcEntityTemplate;

  @Autowired
  SellerService sellerService;

  @Autowired
  ModelMapper modelMapper;

  @Override
  public Mono<Product> findById(Long id) {
    return productRepository.findById(id);
  }

  @Override
  public Mono<Product> insert(Product param) {

    return sellerService.findById(param.getSellerId())
        .switchIfEmpty(Mono.error(
            new ErrorResponse(HttpStatus.NOT_FOUND, "Seller tidak ditemukan")))
        .flatMap(seller -> r2dbcEntityTemplate.insert(param));
  }

  @Override
  public Mono<Product> save(Product param) {
    return findById(param.getId())
        .doOnNext(result -> BeanUtils.copyProperties(param, result,
            "createdDate", "lastModifiedDate"))
        .flatMap(editedResult -> productRepository.save(editedResult));
  }

  @Override
  public Mono<Void> delete(Long id) {
    return productRepository.deleteById(id);
  }

  @Override
  public Mono<Void> deleteBySellerId(Long sellerId) {
    return productRepository.deleteBySellerId(sellerId);
  }

  @Override
  public Flux<Product> getAll(Criteria criteria) {
    return r2dbcEntityTemplate.select(Query.query(criteria), Product.class);
  }

  @Override
  public Mono<Long> getCount(Criteria criteria) {
    return r2dbcEntityTemplate.count(Query.query(criteria), Product.class);
  }

  @Override
  public Flux<ProductDto> getAll(ProductRequest productRequest) {
    Criteria criteria = getCriteria(productRequest);

    return getAll(criteria)
        .skip(productRequest.getSkipRow()).take(productRequest.getPageSize())
        .map(product -> modelMapper.map(product, ProductDto.class));
  }

  @Override
  public Mono<Long> getCount(ProductRequest productRequest) {
    Criteria criteria = getCriteria(productRequest);
    return getCount(criteria);
  }

  private Criteria getCriteria(ProductRequest productRequest) {
    List<Criteria> criteriaList = new ArrayList<>();

    if (StringUtils.isNotBlank(productRequest.getNama())) {
      criteriaList.add(Criteria.where("NAMA").like("%"+productRequest.getNama()+"%"));
    }

    if (productRequest.getSellerId() != null) {
      criteriaList.add(Criteria.where("SELLER_ID").is(productRequest.getSellerId()));
    }

    return Criteria.from(criteriaList);
  }

}
