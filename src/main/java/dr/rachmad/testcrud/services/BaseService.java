package dr.rachmad.testcrud.services;

import org.springframework.data.relational.core.query.Criteria;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BaseService<T> {

  Mono<T> findById(Long id);
  Mono<T> insert(T param);
  Mono<T> save(T param);
  Mono<Void> delete(Long id);
  Flux<T> getAll(Criteria criteria);
  Mono<Long> getCount(Criteria criteria);
}
