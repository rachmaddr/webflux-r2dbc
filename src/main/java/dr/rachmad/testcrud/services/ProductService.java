package dr.rachmad.testcrud.services;

import dr.rachmad.testcrud.dto.ProductDto;
import dr.rachmad.testcrud.entity.Product;
import dr.rachmad.testcrud.request.ProductRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductService extends BaseService<Product> {

  Flux<ProductDto> getAll(ProductRequest productRequest);
  Mono<Long> getCount(ProductRequest productRequest);
  Mono<Void> deleteBySellerId(Long sellerId);
}
