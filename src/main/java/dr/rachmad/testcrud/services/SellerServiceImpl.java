package dr.rachmad.testcrud.services;

import dr.rachmad.testcrud.dao.SellerRepository;
import dr.rachmad.testcrud.dto.SellerDto;
import dr.rachmad.testcrud.entity.Seller;
import dr.rachmad.testcrud.request.SellerRequest;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class SellerServiceImpl implements SellerService {
  @Autowired
  ReactiveTransactionManager transactionManager;

  @Autowired
  R2dbcEntityTemplate r2dbcEntityTemplate;

  @Autowired
  ProductService productService;

  @Autowired
  ModelMapper modelMapper;

  @Autowired
  private SellerRepository sellerRepository;

  @Override
  public Mono<Seller> findById(Long id) {
    return sellerRepository.findById(id);
  }

  @Override
  public Mono<Seller> insert(Seller param) {

    return r2dbcEntityTemplate.insert(param);
  }

  @Override
  public Mono<Seller> save(Seller param) {
    return findById(param.getId())
        .doOnNext(result -> BeanUtils.copyProperties(param, result,
            "createdDate", "lastModifiedDate"))
        .flatMap(editedResult -> sellerRepository.save(editedResult));
  }

  @Override
  public Mono<Void> delete(Long id) {
    TransactionalOperator operator = TransactionalOperator.create(transactionManager);

    return productService.deleteBySellerId(id)
        .then(productService.delete(id))
        .as(operator::transactional);
  }

  @Override
  public Flux<Seller> getAll(Criteria criteria) {
    return r2dbcEntityTemplate.select(Query.query(criteria), Seller.class);
  }

  @Override
  public Mono<Long> getCount(Criteria criteria) {
    return r2dbcEntityTemplate.count(Query.query(criteria), Seller.class);
  }

  @Override
  public Flux<SellerDto> getAll(SellerRequest sellerRequest) {
    Criteria criteria = getCriteria(sellerRequest);

    return getAll(criteria)
        .skip(sellerRequest.getSkipRow()).take(sellerRequest.getPageSize())
        .map(seller -> modelMapper.map(seller, SellerDto.class));
  }

  @Override
  public Mono<Long> getCount(SellerRequest sellerRequest) {
    Criteria criteria = getCriteria(sellerRequest);
    return getCount(criteria);
  }

  private Criteria getCriteria(SellerRequest sellerRequest) {
    List<Criteria> criteriaList = new ArrayList<>();

    if (StringUtils.isNotBlank(sellerRequest.getNama())) {
      criteriaList.add(Criteria.where("NAMA").like("%" + sellerRequest.getNama() + "%"));
    }

    if (StringUtils.isNotBlank(sellerRequest.getKota())) {
      criteriaList.add(Criteria.where("KOTA").like("%" + sellerRequest.getKota() + "%"));
    }

    return Criteria.from(criteriaList);
  }

}
