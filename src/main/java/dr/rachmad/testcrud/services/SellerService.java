package dr.rachmad.testcrud.services;

import dr.rachmad.testcrud.dto.SellerDto;
import dr.rachmad.testcrud.entity.Seller;
import dr.rachmad.testcrud.request.SellerRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SellerService extends BaseService<Seller> {

  Flux<SellerDto> getAll(SellerRequest sellerRequest);
  Mono<Long> getCount(SellerRequest sellerRequest);
}
