package dr.rachmad.testcrud.configuration;

import dr.rachmad.testcrud.utils.ErrorResponse;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.server.ServerRequest;

@Component
public class CustomErrorAttributeConfig extends DefaultErrorAttributes {

  private static final String STATUS_KEY = "status";
  private static final String CODE_KEY = "code";
  private static final String MESSAGE_KEY = "message";
  private static final String DATA_KEY = "data";

  @Override
  public Map<String, Object> getErrorAttributes(ServerRequest request,
      ErrorAttributeOptions options) {
    Map<String, Object> errorAttributes = super.getErrorAttributes(request, options);

    Throwable ex = getError(request);

    if (ex instanceof ErrorResponse) {
      ErrorResponse errorResponse = (ErrorResponse) ex;

      errorAttributes.put(STATUS_KEY, errorResponse.getHttpStatus().value());
      errorAttributes.put(CODE_KEY, errorResponse.getHttpStatus().name());
      errorAttributes.put(MESSAGE_KEY, errorResponse.getMessage());
      errorAttributes.put(DATA_KEY, null);
    }

    if (ex instanceof WebExchangeBindException) {
      WebExchangeBindException webExchangeBindException = (WebExchangeBindException) ex;
      final StringBuilder builder = new StringBuilder();
      webExchangeBindException.getAllErrors()
          .forEach(errorAttributesValidation ->
            builder.append(errorAttributesValidation.getDefaultMessage() + "\n"));

      errorAttributes.put(STATUS_KEY, HttpStatus.BAD_REQUEST.value());
      errorAttributes.put(CODE_KEY, HttpStatus.BAD_REQUEST.name());
      errorAttributes.put(MESSAGE_KEY, builder.toString());
      errorAttributes.put(DATA_KEY, null);
    }

    if (StringUtils.isBlank((String) errorAttributes.get(CODE_KEY))) {
      errorAttributes.put(STATUS_KEY, 500);
      errorAttributes.put(CODE_KEY, "UNHANDLED");
      errorAttributes.put(MESSAGE_KEY, null);
      errorAttributes.put(DATA_KEY, null);
    }

    errorAttributes.remove("path");
    errorAttributes.remove("requestId");
    errorAttributes.remove("timestamp");
    errorAttributes.remove("error");

    return errorAttributes;
  }
}
