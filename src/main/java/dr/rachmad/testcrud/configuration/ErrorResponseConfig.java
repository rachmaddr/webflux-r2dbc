package dr.rachmad.testcrud.configuration;

import dr.rachmad.testcrud.dto.ErrorDto;
import dr.rachmad.testcrud.utils.ErrorResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@RestControllerAdvice
public class ErrorResponseConfig {

//  @ExceptionHandler(ErrorResponse.class)
  public ErrorDto handleError(ErrorResponse errorResponse) {
    return ErrorDto.builder()
        .code(errorResponse.getHttpStatus().value())
        .message(errorResponse.getMessage())
        .status(errorResponse.getHttpStatus().name())
        .build();
  }
}
