CREATE SCHEMA `test` DEFAULT CHARACTER SET latin1 ;

CREATE TABLE `SELLER_TBL` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAMA` varchar(255) DEFAULT NULL,
  `ALAMAT` varchar(255) DEFAULT NULL,
  `KOTA` varchar(255) DEFAULT NULL,
  `NOMOR_TELP` varchar(20) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `last_modified_date` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `nameAndKota` (`NAMA`,`KOTA`),
  KEY `kota` (`KOTA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `PRODUCT_TBL` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAMA` varchar(255) DEFAULT NULL,
  `SELLER_ID` bigint(20) DEFAULT NULL,
  `SATUAN` varchar(50) DEFAULT NULL,
  `HARGA_SATUAN` double DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `last_modified_date` datetime NOT NULL,
  `PRODUK_TBLcol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `namaAndSellerId` (`NAMA`,`SELLER_ID`),
  KEY `sellerId` (`SELLER_ID`),
  CONSTRAINT `sellerIdForeign` FOREIGN KEY (`SELLER_ID`) REFERENCES `SELLER_TBL` (`ID`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

