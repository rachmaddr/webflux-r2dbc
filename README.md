# Kebutuhan Sistem Minimal
* openJDK 8
* Mysql 5
* Maven (ketika melakukan build `aplikasi`)

# Instalasi
* import file `init.sql` pada folder `database`, ke dalam Mysql, untuk memasukan struktur database, dan tabel yang dibutuhkan.
* lakukan penyesuaian pada file `application.properties` yang ada di folder `src\resources`, pada koneksi database Mysql
* lakukan `build` aplikasi, untuk mendapatkan file _binary_ dengan nama *test-crud-0.0.1-SNAPSHOT.jar* , dengan perintah maven `mvn clean install`
* untuk menjalankan aplikasi, kita dapat menggunakan perintah `java -jar test-crud-0.0.1-SNAPSHOT.jar` 

# Akses Aplikasi
Aplikasi dapat diakses melalui `Swagger` dan `Postman`.
* Untuk `Swagger` dapat diakses melalui `http:\\localhost:8080\swagger-ui.html`
* Untuk melalui aplikasi `Postman`, dapat melakukan import file *Test.postman_collection.json* sebagai `collection`.